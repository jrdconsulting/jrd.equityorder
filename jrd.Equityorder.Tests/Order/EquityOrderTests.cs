﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using FakeItEasy;
using jrd.Equityorder.Core.Interfaces;
using jrd.Equityorder.Core.Interfaces.Error;
using jrd.Equityorder.Core.Order;
using NUnit.Framework;

namespace jrd.Equityorder.Tests.Order
{
    [TestFixture]
    public class EquityOrderTests
    {
        EquityOrder equityOrder;
        string knownCode;
        int quantity;
        Threshold threshold;
        IOrderService orderService;

        [SetUp]
        public void BeforeEveryTest()
        {
            orderService = A.Fake<IOrderService>();
            knownCode = "ou39uyjhvsk";
            quantity = 100;
            threshold = new Threshold(34.688m, AboveOrBelow.Below);  // always below in these tests, due to requirement
            equityOrder = new EquityOrder(knownCode, threshold, quantity, orderService);
        }

        [Test]
        public void WhenThereIsATickForAnEquityThatMatchesAndIsBelowTheThreshold()
        {
            // act
            equityOrder.ReceiveTick(knownCode, 34);

            A.CallTo(() => orderService.Buy(knownCode, quantity, 34)).MustHaveHappened();
        }

        [Test]
        public void WhenThereIsATickForAnEquityThatMatchesButThePriceIsNotBelowTheThreshold()
        {
            // act
            equityOrder.ReceiveTick(knownCode, 35);

            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).MustNotHaveHappened();
        }

        [Test]
        public void WhenThereIsATickForAnEquityThatDoesNotMatch()
        {
            // act
            equityOrder.ReceiveTick("jnjsd", 10);

            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).MustNotHaveHappened();
        }

        [Test]
        public void WhenThereIsABuyItShouldSignalTheOrderHasBeenPlacedWhenSomeoneIsListening()
        {
            OrderPlacedEventArgs orderPlacedSignal = null;
            equityOrder.OrderPlaced += e => orderPlacedSignal = e;

            // act
            equityOrder.ReceiveTick(knownCode, 10.8m);

            orderPlacedSignal.ShouldNotBeNull();
            orderPlacedSignal.EquityCode.ShouldEqual(knownCode);
            orderPlacedSignal.Price.ShouldEqual(10.8m);
        }

        [Test]
        public void WhenThereIsABuyShouldNotBuyAgainWhenItTicksASecondTime()
        {
            var orderPlacedSignals = new List<OrderPlacedEventArgs>();
            equityOrder.OrderPlaced += e => orderPlacedSignals.Add(e);

            equityOrder.ReceiveTick(knownCode, 1.1m);

            // act
            equityOrder.ReceiveTick(knownCode, 21.2m);

            //original should have happened
            A.CallTo(() => orderService.Buy(knownCode, quantity, 1.1m)).MustHaveHappened();
            //should be no second order
            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).MustHaveHappenedOnceExactly();

            orderPlacedSignals.Count.ShouldEqual(1);
        }

        [Test]
        public void WhenThereAreMultipleThreadsHittingTheSameInstanceWithATickShouldOnlyBuyTheOnce()
        {
            var gate = new ManualResetEvent(false);
            
            int started = 0;
            var tasks = Enumerable.Range(1, 10).Select(i =>
            {
                return Task.Run(() =>
                {
                    Interlocked.Increment(ref started);
                    gate.WaitOne(30000);                    // make them all wait at the gate, ready to go with a tick
                    equityOrder.ReceiveTick(knownCode, i);
                });
            }).ToArray();

            //wait for them all to start
            while (started < 10)
            {
                Thread.Sleep(10);
            }

            // now release them to fall over each other
            gate.Set();

            // wait for all to finish
            Task.WaitAll(tasks);

            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void WhenThereIsAnErrorBuyingShouldNotProcessAnyMoreTicks()
        {
            var exception = new Exception("unit testing exception");
            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).Throws(exception);

            // act
            equityOrder.ReceiveTick(knownCode, 9.9m);

            equityOrder.ReceiveTick(knownCode, 19.1m);

            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).MustHaveHappenedOnceExactly();
        }

        [Test]
        public void WhenThereIsAnErrorBuyingShouldSignalThereWasAProblemWhenSomeoneIsListening()
        {
            OrderErroredEventArgs errorSignal = null;
            equityOrder.OrderErrored += e => errorSignal = e;

            var exception = new Exception("unit testing exception");
            A.CallTo(() => orderService.Buy(A<string>.Ignored, A<int>.Ignored, A<decimal>.Ignored)).Throws(exception);

            // act
            equityOrder.ReceiveTick(knownCode, 9.9m);
            
            errorSignal.ShouldNotBeNull();
            errorSignal.GetException().ShouldEqual(exception);
            errorSignal.EquityCode.ShouldEqual(knownCode);
            errorSignal.Price.ShouldEqual(9.9m);
        }
    }
}
