﻿namespace jrd.Equityorder.Core.Interfaces
{
    public interface IOrderPlaced
    {
        event OrderPlacedEventHandler OrderPlaced;
    }
}