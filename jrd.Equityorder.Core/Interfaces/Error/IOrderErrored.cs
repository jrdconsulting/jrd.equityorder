﻿namespace jrd.Equityorder.Core.Interfaces.Error
{
    public interface IOrderErrored
    {
        event OrderErroredEventHandler OrderErrored;
    }
}