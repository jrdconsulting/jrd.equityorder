﻿namespace jrd.Equityorder.Core.Order
{
    public enum AboveOrBelow
    {
        Unknown,
        Above,
        Below
    }
}