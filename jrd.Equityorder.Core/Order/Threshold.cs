﻿namespace jrd.Equityorder.Core.Order
{
    public class Threshold
    {
        public Threshold(decimal price, AboveOrBelow direction)
        {
            Price = price;
            Direction = direction;
        }

        public decimal Price { get; }
        public AboveOrBelow Direction { get; }
    }
}