﻿using System;
using jrd.Equityorder.Core.Interfaces;
using jrd.Equityorder.Core.Interfaces.Error;

namespace jrd.Equityorder.Core.Order
{
    public class EquityOrder : IEquityOrder, IDisposable
    {
        readonly object padlock = new object();   // not static, each instance can buy, but buy only once with this lock
        readonly string codeInterestedIn;
        readonly Threshold threshold;
        readonly int size;
        readonly IOrderService orderService;
        bool ignoreTicks;

        public EquityOrder(string equityCode, Threshold threshold, int size, IOrderService orderService)
        {
            this.codeInterestedIn = equityCode;
            this.threshold = threshold;
            this.size = size;
            this.orderService = orderService;
        }

        public void ReceiveTick(string equityCode, decimal price)
        {
            if (ignoreTicks)
            {
                return;
            }

            if (!equityCode.Equals(codeInterestedIn))
            {
                return;
            }

            if (price < threshold.Price && threshold.Direction == AboveOrBelow.Below)
            {
                Exception problem = null;
                bool bought = false;

                lock (padlock)
                {
                    // double-check for any slipping the lock after this exits
                    if (!ignoreTicks)
                    {
                        try
                        {
                            orderService.Buy(equityCode, size, price);
                            bought = true;
                        }
                        catch (Exception e)
                        {
                            problem = e;
                        }
                        finally
                        {
                            this.Dispose();
                        }
                    }
                }

                // important to have events outside the lock, so we control it internally before calling out
                if (bought)
                {
                    OrderPlaced?.Invoke(new OrderPlacedEventArgs(equityCode, price));
                }

                if (problem != null)
                {
                    OrderErrored?.Invoke(new OrderErroredEventArgs(equityCode, price, problem));
                }
            }
        }

        public event OrderPlacedEventHandler OrderPlaced;

        public event OrderErroredEventHandler OrderErrored;

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                ignoreTicks = true;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
